package pl.terada.learn.esgame.flow;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ObjectMethod {
    private final Object invokable;
    private final Method method;

    public ObjectMethod(Object invokable, Method method) {
        this.invokable = invokable;
        this.method = method;
    }

    public void invoke(Command event) throws InvocationTargetException, IllegalAccessException {
        method.invoke(this.invokable, event);
    }

    public Object getInvokable() {
        return invokable;
    }

    public Method getMethod() {
        return method;
    }
}
