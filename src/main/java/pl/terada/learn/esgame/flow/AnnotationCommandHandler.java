package pl.terada.learn.esgame.flow;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class AnnotationCommandHandler implements ApplicationContextAware, InitializingBean, CommandHandler {

    private ApplicationContext context;
    private Map<Class<? extends Command>, Collection<ObjectMethod>> map = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, Object> annotatedObjects = context.getBeansWithAnnotation(MagicAnnotationForCommandHandler.class);
        Map<Class<? extends Command>, List<ObjectMethod>> map = annotatedObjects.values().stream()
                .map((o) -> expandMethodsForObject(o).apply(o))
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(
                        getCommandType(),
                        Lists::newArrayList,
                        mergeLists()
                ));
        this.map.putAll(map);
    }

    private static Function<ObjectMethod, Class<? extends Command>> getCommandType() {
        return (m) -> (Class<? extends Command>) m.getMethod().getParameterTypes()[0];
    }

    private static BinaryOperator<List<ObjectMethod>> mergeLists() {
        return (a, b) -> {
            List<ObjectMethod> result = Lists.newArrayList(a);
            result.addAll(b);
            return result;
        };
    }

    private static Function<Object, List<ObjectMethod>> expandMethodsForObject(Object obj) {
        return methodsAcceptingCommand()
                .andThen(compoundMethodsWithObject(obj));
    }

    private static Function<Object, List<Method>> methodsAcceptingCommand() {
        return (obj) -> Stream.of(obj.getClass().getMethods())
                .filter(method -> method.getParameterCount() == 1)
                .filter(method -> Command.class.isAssignableFrom(method.getParameterTypes()[0]))
                .collect(toList());
    }

    private static Function<List<Method>, List<ObjectMethod>> compoundMethodsWithObject(Object obj) {
        return (methods) -> methods.stream()
                .map(method -> new ObjectMethod(obj, method))
                .collect(toList());
    }

    @Override
    public void propagateCommand(Command command) {
        map.keySet().stream()
                .filter((clazz) -> clazz.isAssignableFrom(command.getClass()))
                .map(map::get)
                .flatMap(Collection::stream)
                .parallel()
                .forEach((objectMethod -> {
                    try {
                        objectMethod.invoke(command);
                    } catch (InvocationTargetException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }));
    }

    @VisibleForTesting
    protected final Map<Class<? extends Command>, Collection<ObjectMethod>> getMap() {
        return map;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

}
