package pl.terada.learn.esgame.flow;

public interface CommandHandler {
    void propagateCommand(Command command);
}
