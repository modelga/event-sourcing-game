package pl.terada.learn.esgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import pl.terada.learn.esgame.es.EventStore;
import pl.terada.learn.esgame.es.InMemoryEventStore;

@EnableAutoConfiguration
@ComponentScan
@Configuration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public EventStore globalEventStore() {
        return new InMemoryEventStore();
    }

}
