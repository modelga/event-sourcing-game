package pl.terada.learn.esgame.es;

import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class InMemoryEventStream implements EventStream {
    private final long version;
    private final List<Event> events;


    InMemoryEventStream(long version, List<Event> events) {
        this.version = version;
        this.events = events;
    }

    public static EventStream newStream() {
        return new InMemoryEventStream(0, Collections.emptyList());
    }

    @Override
    public EventStream append(List<? extends Event> newEvents) {
        LinkedList<Event> events = Lists.newLinkedList(this.events);
        events.addAll(newEvents);
        return new InMemoryEventStream(version() + 1, Collections.unmodifiableList(events));
    }

    @Override
    public long version() {
        return version;
    }

    @Override
    public Iterator<Event> iterator() {
        return events.iterator();
    }
}
