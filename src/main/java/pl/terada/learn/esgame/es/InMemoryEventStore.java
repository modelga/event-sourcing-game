package pl.terada.learn.esgame.es;

import com.google.common.collect.Maps;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static pl.terada.learn.esgame.es.InMemoryEventStream.newStream;

public class InMemoryEventStore implements EventStore {

    private final Map<UUID, EventStream> streams;

    public InMemoryEventStore() {
        this.streams = Maps.newConcurrentMap();
    }

    @Override
    public EventStream loadEventStream(UUID uuid) {
        return streams.computeIfAbsent(uuid, (id) -> newStream());

    }

    @Override
    public void store(UUID uuid, Long version, List<? extends Event> events) {
        EventStream stream = loadEventStream(uuid);
        if (stream.version() != version) {
            throw new ConcurrentModificationException("Version mismatch");
        }
        streams.put(uuid, stream.append(events));
    }
}
