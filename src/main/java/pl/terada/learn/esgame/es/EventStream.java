package pl.terada.learn.esgame.es;

import java.util.List;

public interface EventStream extends Iterable<Event> {

    EventStream append(List<? extends Event> newEvents);

    long version();
}
