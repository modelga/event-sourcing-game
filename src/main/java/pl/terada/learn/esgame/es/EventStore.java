package pl.terada.learn.esgame.es;

import java.util.List;
import java.util.UUID;

public interface EventStore {

    EventStream loadEventStream(UUID uuid);

    void store(UUID uuid, Long version, List<? extends Event> events);
}
