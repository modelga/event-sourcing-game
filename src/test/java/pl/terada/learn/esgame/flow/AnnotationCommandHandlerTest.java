package pl.terada.learn.esgame.flow;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;

public class AnnotationCommandHandlerTest {

    private AnnotationCommandHandler handler;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = Mockito.mock(ApplicationContext.class);
        handler = new AnnotationCommandHandler();
        handler.setApplicationContext(context);

        when(context.getBeansWithAnnotation(MagicAnnotationForCommandHandler.class))
                .thenReturn(
                        Stream.of(new SampleCommandHandler())
                                .collect(Collectors.toMap(Object::toString, o -> o))
                );
    }

    @Test
    public void testAfterPropertiesSet() throws Exception {
        // when
        handler.afterPropertiesSet();

        // then
        Assert.assertTrue(handler.getMap().keySet().size() > 0);
    }

    /**
     * weird method of checking because it uses lambda's
     *
     * @throws Exception
     */
    @Test

    public void testPropagateCommand() throws Exception {
        // given
        handler.afterPropertiesSet();

        // then
        try {
            handler.propagateCommand(new SampleCommand());
        } catch (Throwable e) {
            Assert.assertTrue(e.getCause().getCause() instanceof MethodInvokedProperly);
            // pass test here
            return;
        }
        Assert.fail("Handler was not invoked correctly");
    }

    static class SampleCommandHandler {
        public void handleAnCommand(SampleCommand command) {
            throw new MethodInvokedProperly();
        }
    }

    static class SampleCommand extends Command {
    }

    static class MethodInvokedProperly extends RuntimeException {

    }
}
